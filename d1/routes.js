const http = require('http');


// Create a variable "port" to store the port number
const port = 4000

// Create a variable ' server' that stores the output of the 'create server' method
// const server = http.createServer((req, res) => {

// 	if (req.url == '/greeting'){
// 		res.writeHead(200, {'Content-Type':'text/plain'})
// 		res.end('Hello Again')

// 	}
// });
	// Access the /homepage routes returns a message of "This is the homepage" with a 200 status

const server = http.createServer((req, res) => {
	if (req.url == '/homepage'){
		res.writeHead(200, {'Content-Type':'text/plain'})
		res.end('This is the homepage')	
	}
	// All other routes will return a message "Page not found" with a status of 404
	else{
		res.writeHead(404,{'Content-Type': 'text/plain'})
		res.end('Page not found')
	}

});


// Use the 'server' and 'port' variables crated above
server.listen(port);

// when server is running, console will print the message
console.log(`Server now accessible at localhost:${port}`);

// http://localhost:4000/home/about/contacts


