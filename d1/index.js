// require() use the require directive to load Node.js Modules
// "module is a software" component or part of a program that contains one or more routines
// 'http module' - lets Node.js transfer data using HyperText Transfer Protocal. It is a set of individual files that contain code to create a "component" that helps establish data transfer between applications
let http = require("http");
// Clients (browser) and servers (nodeJS/express js application) communicate by exchanging individual messages.
// message sent by the client, usually a web browser are called request
// http://home
// message sent by the server as an answer are called responses

// createServer() method - used to create an HTTP server that listens to requests
// it accepts a function and allows us to perform a certain task for our server
// it accepts a function and allows us to perform a certain task for our server
// http.createServer(function(request, response) {}).listen()
// netstat -a
http.createServer(function(request, response) {
	// use the writeHead() method to:
	// set a  status code for the response - 200 means OK (successful)
	// set the content-type of the response as a plain text message
	response.writeHead(200, {'Content-Type': 'application/json'});
	// We used the response end() method to end the response process
	response.end('Goodbye')
}).listen(4000)
// A port is a virtual point where netowrk connections start and end
// Each port is associated with a specific process or service
// The server will be assigned to port 4000 via the "listen(4000)" method where the server will isten to any requests that are sent to our server
console.log('Server running at localhost:4000')
// http://localhost:4000
// 