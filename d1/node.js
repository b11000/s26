// Client-server architecture
// 
// Laptop   ==\
// Smartphone \Clients ----- Internet ----- Server
// PC       ==\
// 
// Benefits:
// Centralized data makes applications more scalable and maintainable
// Multiple client apps may all use dynamically-generated data
// Workload is concentrated on the server, making client apps
// Improves data availability
// 
// Node.js
// An open-source, Javascript runtime evnironment for creating server-side applications.
// 
// Runtime Environment
// Gives the context for running a programming language
// JS was initially within the context of the browser, giving it access to:
// 
// 
// Runtime Environment
// With Node.js, the context was taken out of the browser and put into the server
// 
// With Node.js javascript now has access to the following:
// 	System resources
// 	Memory
// 	File System
// 
// 
// 
// Benefits
// 	Performance
// 		Optimized for web applications
// 	Familiarity
// 		"Same old" Javascript
// Access to Node Package Manager (NPM)
// 		"World's largest Packages"
// 
// 
// 
// 
// 
// 
// 




